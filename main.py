from flask import Flask, request, jsonify
from flask_restful import Api, Resource

from myPackage.livekit_custom import Livekit

app = Flask(__name__)
api = Api(app)


# class returnToken(Resource):
#     def get(self):
#         args = request.args
#         print(args)  # For debugging
#         no1 = args['join']
#         no2 = args['roomName']
#         no3 = args['userName']
#         #token = create_token(join=no1, roomName=no2, userName=no3)
#         token = livekit.create_token(join=no1, roomName=no2, userName=no3)
#         return jsonify(dict(data=token))
#
#
# api.add_resource(returnToken, '/createToken')
# class returnToken(Resource):
#     def get(self):
#         json = request.json
#         token = livekit.create_token(
#             room_create=bool(json["room_create"]),
#             join=bool(json["join"]),
#             room_list=bool(json["room_list"]),
#             room_record=bool(json["room_record"]),
#             room_admin=json["room_admin"],
#             roomName=json["roomName"],
#             can_publish=bool(json["can_publish"]),
#             can_subscribe=bool(json["can_subscribe"]),
#             can_publish_data=bool(json["can_publish"]),
#             hidden=bool(json["hidden"]),
#             userName=json["userName"],
#             metadata=json["metadata"])
#         #token = livekit.create_token(join=bool(json['join']), roomName=json['roomName'], userName=json['userName'])
#         return jsonify(dict(data=token))
class Home(Resource):
    def get(self):
        data = {"hi":"port working"}
        return jsonify(data)

api.add_resource(Home, '/')


class returnToken(Resource):
    def get(self):
        json = request.json
        token = livekit.create_token(bool(json["join"]), json["roomName"], json["userName"])
        return token

api.add_resource(returnToken, '/createToken')


class createRoom(Resource):
    def post(self):
        # content_type = request.headers.get('Content-Type')
        # if (content_type == 'application/json'):
        json = request.json
        roomInfo = livekit.create_room(json['roomName'], json['maxTime'], json['maxUser'])
        return jsonify(dict(data=str(roomInfo)))


api.add_resource(createRoom, '/createRoom')


class roomList(Resource):
    def get(self):
        roomList = livekit.room_list()
        print(roomList)
        if roomList is None:
            roomList = "hi"
        return jsonify(dict(data=str(roomList)))


api.add_resource(roomList, '/roomList')


class deleteRoom(Resource):
    def post(self):
        json = request.json
        response = livekit.delete_room(json["roomName"])
        return jsonify(dict(data=str(response)))


api.add_resource(deleteRoom, '/deleteRoom')


class listParticipants(Resource):
    def get(self):
        json = request.json
        response = livekit.list_participants(json["roomName"])
        return jsonify(dict(data=str(response)))


api.add_resource(listParticipants, '/listParticipants')


class getParticipant(Resource):
    def get(self):
        json = request.json
        try:
            response = livekit.get_participant(json["roomName"], json["userName"])
        except:
            response = "user doesn't exist"
        print()
        return jsonify(dict(data=str(response)))


api.add_resource(getParticipant, '/getParticipant')


class mutePublishedTrack(Resource):
    def post(self):
        json = request.json
        try:
            response = livekit.mute_published_track(json["roomName"], json["userName"], json["track_sid"], bool(json["muted"]))
        except:
            response = "check user, track or room's value"
        return jsonify(dict(data=str(response)))


api.add_resource(mutePublishedTrack, '/mutePublishedTrack')

#permision bolood metadata nii type heregtei
class updateMetaData(Resource):
    def post(self):
        json = request.json
        # try:
        #     response = livekit.update_participant(json["roomName"], json["userName"], json["metadata"], bool(json["permission"]))
        # except:
        #     response = "check user or room's value"
        response = livekit.update_participant(json["roomName"], json["userName"])
        return jsonify(dict(data=str(response)))


api.add_resource(updateMetaData, '/updateMetaData')


class updateSubscription(Resource):
    def post(self):
        json = request.json
        try:
            response = livekit.update_participant(json["userName"], json["roomName"], json["track_sids"], bool(json["subscribe"]))
        except:
            response = "check user , track_ids or room's value"
        return jsonify(dict(data=str(response)))


api.add_resource(updateSubscription, '/updateSubscription')


class updateRoomMetaData(Resource):
    def post(self):
        json = request.json
        try:
            response = livekit.update_room_metadata(json["roomName"], json["userName"], )
        except:
            response = "the room you requested doesn't exist"
        return jsonify(dict(data=str(response)))


api.add_resource(updateRoomMetaData, '/updateRoomMetaData')


class sendData(Resource):
    def post(self):
        json = request.json
        # try:
        #     response = livekit.update_room_metadata(json["roomName"], json["userName"], )
        # except:
        #     response = "the room you requested doesn't exist"
        response = livekit.send_data(json["roomName"], json["bytes"], json["kind"], json["destination_sids"], )
        return jsonify(dict(data=str(response)))


api.add_resource(sendData, '/sendData')

if __name__ == '__main__':
    livekit = Livekit()
    from waitress import serve
    serve(app, host="127.0.0.1", port=5000)
    # app.run(debug=True)
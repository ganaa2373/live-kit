from livekit import AccessToken, VideoGrant, RoomServiceClient, DEFAULT_TOKEN_TTL


class Livekit(object):
    def __init__(self):
        self.client = RoomServiceClient("http://localhost:7880", "devkey", "secret")

    # def create_token(
    #         self,
    #         room_create=None,
    #         join=None,
    #         room_list=None,
    #         room_record=None,
    #         room_admin=None,
    #         roomName="",
    #         can_publish=None,
    #         can_subscribe=None,
    #         can_publish_data=None,
    #         hidden=None,
    #         api_key="devkey",
    #         api_secret="secret",
    #         userName="",
    #         ttl=DEFAULT_TOKEN_TTL,
    #         metadata=None
    # ):
    #     grant = VideoGrant(
    #         room_create, join, room_list,
    #         room_record, room_admin, roomName,
    #         can_publish, can_subscribe, can_publish_data,hidden,
    #     )
    #     access_token = AccessToken(
    #         api_key=api_key, api_secret=api_secret,
    #         grant=grant, identity=userName,
    #         metadata=metadata, ttl=ttl)
    #     token = access_token.to_jwt()
    #     return (token)
    def create_token(self, join=False, roomName='room1', userName=''):
        grant = VideoGrant(
            room_join=join, room=roomName, room_admin=True
        )
        # access_token = AccessToken("devkey", "secret", grant=grant, identity="Bob")
        access_token = AccessToken("devkey", "secret", grant=grant, identity=userName,)
        token = access_token.to_jwt()
        return (token)

    def create_room(self, roomName='room', maxTime=30, maxUser=3):
        return self.client.create_room(roomName, maxTime, maxUser)

    def room_list(self):
        return self.client.list_rooms()

    def delete_room(self, roomName=""):
        print(roomName)
        return self.client.delete_room(roomName)

    def list_participants(self, roomName=""):
        response = self.client.list_participants(roomName)
        return response

    def get_participant(self, roomName="", userName=""):
        return self.client.get_participant(roomName, userName)

    def remove_participant(self, roomName="", userName=""):
        return self.client.remove_participant(roomName, userName)

    def mute_published_track(self, roomName="", userName="", track_sid="", muted="True"):
        return self.client.mute_published_track(roomName, userName, track_sid, muted)

    def update_participant(self, roomName="", userName="", metadata="updated metadata",):
        #return self.client.update_participant(roomName, userName,metadata= {"livekit":"hi"}, permission={"livekit":"hi"})
        return self.client.update_participant(roomName, userName, metadata)

    def update_subscription(self, userName="", roomName="", track_sids=list[""], subscribe=False, ):
        return self.client.update_subscriptions(userName, roomName, track_sids, subscribe)

    def update_room_metadata(self, roomName="", metadata=""):
        return self.client.update_room_metadata(roomName, metadata)

    def send_data(self, roomName="", bytes="", kind="", destination_sids=[""]):
        return self.client.send_data(roomName, bytes, kind, destination_sids)